from Taxi.importer import format_columns_for_query, generic_importer
from Taxi.k_means import get_coordinates, Trip

if __name__ == "__main__":
    # total_rows = 0
    # for _ in get_coordinates():
    #     total_rows += 1

    total_rows = 1658567

    print("%d records to insert" % total_rows)

    rows = get_coordinates()

    columns = format_columns_for_query(Trip)
    generic_importer("taxi_trip_coordinates", columns, rows, total_rows)
