from Taxi.polyline_parser import parse_polyline, GPSPoint


def test_2_points():
    polyline_str = "[[1.22, 3.44], [5.66, 7.88]]"
    polyline = list(parse_polyline(polyline_str))

    assert polyline[0] == GPSPoint(
        longitude=1.22,
        latitude=3.44
    )
    assert polyline[1] == GPSPoint(
        longitude=5.66,
        latitude=7.88
    )


def test_long_polyline():
    polyline_str = \
        "[[-8.618643,41.141412]," \
        "[-8.618499,41.141376]," \
        "[-8.620326,41.14251]," \
        "[-8.622153,41.143815]," \
        "[-8.623953,41.144373]," \
        "[-8.62668,41.144778]," \
        "[-8.627373,41.144697]," \
        "[-8.630226,41.14521]," \
        "[-8.632746,41.14692]," \
        "[-8.631738,41.148225]," \
        "[-8.629938,41.150385]," \
        "[-8.62911,41.151213]," \
        "[-8.629128,41.15124]," \
        "[-8.628786,41.152203]," \
        "[-8.628687,41.152374]," \
        "[-8.628759,41.152518]," \
        "[-8.630838,41.15268]," \
        "[-8.632323,41.153022]," \
        "[-8.631144,41.154489]," \
        "[-8.630829,41.154507]," \
        "[-8.630829,41.154516]," \
        "[-8.630829,41.154498]," \
        "[-8.630838,41.154489]]"

    polyline = list(parse_polyline(polyline_str))
    assert polyline == [GPSPoint(longitude=-8.618643, latitude=41.141412),
                        GPSPoint(longitude=-8.618499, latitude=41.141376),
                        GPSPoint(longitude=-8.620326, latitude=41.14251),
                        GPSPoint(longitude=-8.622153, latitude=41.143815),
                        GPSPoint(longitude=-8.623953, latitude=41.144373),
                        GPSPoint(longitude=-8.62668, latitude=41.144778),
                        GPSPoint(longitude=-8.627373, latitude=41.144697),
                        GPSPoint(longitude=-8.630226, latitude=41.14521),
                        GPSPoint(longitude=-8.632746, latitude=41.14692),
                        GPSPoint(longitude=-8.631738, latitude=41.148225),
                        GPSPoint(longitude=-8.629938, latitude=41.150385),
                        GPSPoint(longitude=-8.62911, latitude=41.151213),
                        GPSPoint(longitude=-8.629128, latitude=41.15124),
                        GPSPoint(longitude=-8.628786, latitude=41.152203),
                        GPSPoint(longitude=-8.628687, latitude=41.152374),
                        GPSPoint(longitude=-8.628759, latitude=41.152518),
                        GPSPoint(longitude=-8.630838, latitude=41.15268),
                        GPSPoint(longitude=-8.632323, latitude=41.153022),
                        GPSPoint(longitude=-8.631144, latitude=41.154489),
                        GPSPoint(longitude=-8.630829, latitude=41.154507),
                        GPSPoint(longitude=-8.630829, latitude=41.154516),
                        GPSPoint(longitude=-8.630829, latitude=41.154498),
                        GPSPoint(longitude=-8.630838, latitude=41.154489)]
