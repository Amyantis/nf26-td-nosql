from collections import namedtuple

from Taxi.importer import _make_query
from Taxi.importer import format_columns_for_query


def test_format_columns_for_query():
    Obj = namedtuple("Obj", ["A", "B"])
    assert format_columns_for_query(Obj) == "(A, B)"


def test_make_query():
    query = _make_query("(A, B)", "table_name")
    assert query == "INSERT INTO table_name (A, B) VALUES (?, ?)"
