from collections import namedtuple

from Taxi.importer import format_columns_for_query, generic_importer
from Taxi.polyline_distance import compute_distance
from Taxi.reader_taxi import parse_file, number_of_lines

Row = namedtuple("Row", [
    "taxi_id",
    "trip_year",
    "trip_month",
    "trip_day",
    "trip_timestamp",
    "trip_id",
    "call_type",
    "origin_call",
    "origin_stand",
    "day_type",
    "missing_data",
    "polyline",
    "starting_point_latitude",
    "starting_point_longitude",
    "ending_point_latitude",
    "ending_point_longitude",
    "distance",
])


def make_rows_for_insertion():
    for trip in parse_file():
        polyline = \
            [(point.latitude, point.longitude) for point in trip.polyline]
        try:
            distance = compute_distance(polyline)
        except Exception as exception:
            raise Exception(exception, polyline)

        if len(polyline) > 0:
            starting_point_latitude = polyline[0][0]
            starting_point_longitude = polyline[0][1]
            ending_point_latitude = polyline[-1][0]
            ending_point_longitude = polyline[-1][1]
        else:
            starting_point_latitude = 0.0
            starting_point_longitude = 0.0
            ending_point_latitude = 0.0
            ending_point_longitude = 0.0

        yield Row(
            taxi_id = trip.taxi_id,
            trip_year = trip.timestamp.year,
            trip_month = trip.timestamp.month,
            trip_day = trip.timestamp.day,
            trip_timestamp = trip.timestamp,
            trip_id = trip.trip_id,
            call_type = trip.call_type,
            origin_call = trip.origin_call,
            origin_stand = trip.origin_stand,
            day_type = trip.day_type,
            missing_data = trip.missing_data,
            polyline = str(list(trip.polyline)),
            starting_point_latitude = starting_point_latitude,
            starting_point_longitude = starting_point_longitude,
            ending_point_latitude = ending_point_latitude,
            ending_point_longitude = ending_point_longitude,
            distance = distance,
        )


if __name__ == "__main__":
    rows = make_rows_for_insertion()
    columns = format_columns_for_query(Row)
    total_rows = number_of_lines() - 1
    print("%d to insert" % total_rows)
    generic_importer("taxi_trip", columns, rows, total_rows)
