import itertools

from cassandra.cluster import Cluster
from cassandra.query import BatchStatement
from etaprogress.progress import ProgressBar

CHUNK_SIZE = 100


def generic_importer(table_name, columns, rows, total_rows=-1):
    cluster = Cluster(connect_timeout=60)
    session = cluster.connect()

    session.set_keyspace('taxi')

    query = _make_query(columns, table_name)

    print(query)

    prepared_statement = session.prepare(query)

    batch = BatchStatement()

    if total_rows > 0:
        bar = ProgressBar(total_rows, max_width=100)
        _update_progress_bar(bar, 0)

    while True:
        batch.clear()
        inserted = 0

        for row in itertools.islice(rows, CHUNK_SIZE):
            batch.add(prepared_statement, row)
            inserted += 1

        if inserted == 0:
            break

        session.execute(batch)

        if total_rows > 0:
            _update_progress_bar(bar, inserted)


def _update_progress_bar(bar, inserted):
    bar.numerator += inserted
    print(bar)


def format_columns_for_query(namedtuple_associated):
    return str(namedtuple_associated._fields).replace("'", "")


def _make_query(columns, table_name):
    specifiers = _make_specifiers(columns)
    query = \
        "INSERT INTO {table_name} {columns} VALUES ({specifiers})". \
            format(table_name=table_name, columns=columns, specifiers=specifiers)
    return query


def _make_specifiers(columns):
    specifiers = ""
    for _ in columns.split(sep=","):
        specifiers += "?, "
    specifiers = specifiers[:-2]
    assert specifiers != ""
    return specifiers
