import json
from collections import namedtuple

GPSPoint = namedtuple(
    "GPSPoint",
    ["longitude", "latitude"]
)


def parse_polyline(polyline_str):
    for position in json.loads(polyline_str):
        yield GPSPoint(
            longitude=float(position[0]),
            latitude=float(position[1])
        )
