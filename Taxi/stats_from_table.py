import pandas as pandas
from cassandra.cluster import Cluster
from cassandra.query import named_tuple_factory


def get_rows_from_db(query):
    cluster = Cluster()
    session = cluster.connect()
    session.set_keyspace('taxi')
    session.row_factory = named_tuple_factory
    rows = session.execute(query)
    return rows


if __name__ == "__main__":
    rows = get_rows_from_db(query="SELECT * FROM taxi_trip_shorted")

    data_frame = pandas.DataFrame(data=list(rows), columns=rows.column_names)

    data_frame.sort(['trip_year', 'trip_month'], ascending=[True, True])

    month_distance__mean = data_frame.groupby(['trip_year', 'trip_month'])["distance"].mean()
    print(month_distance__mean)
