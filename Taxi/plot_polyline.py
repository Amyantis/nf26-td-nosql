import itertools

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

from Taxi.reader_taxi import parse_file

llcrnrlon = -8.6545244
llcrnrlat = 41.165843
urcrnrlon = -8.5916075
urcrnrlat = 41.1400853

# resolution = 'f' # full
resolution = 'l'  # low

basemap = Basemap(projection='merc',
                  # lat_0=(llcrnrlat + urcrnrlat) / 2,
              # lon_0=(llcrnrlon + urcrnrlon) / 2,
              llcrnrlon=llcrnrlon,
                  llcrnrlat=llcrnrlat,
                  urcrnrlon=urcrnrlon,
                  urcrnrlat=urcrnrlat,
                  resolution=resolution,
                  epsg=3763)

basemap.arcgisimage(service='World_Street_Map',
                    xpixels=5000,
                    verbose=False)

for completed_trip in itertools.islice(parse_file(), 500):
    positions = [(point.longitude, point.latitude) for point in completed_trip.polyline]
    longitudes, latitudes = zip(*positions)

    x, y = basemap(longitudes, latitudes)
    basemap.plot(x, y)

plt.title("Taxi Service Trajectories")
plt.show()
