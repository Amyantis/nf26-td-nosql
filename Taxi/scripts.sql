CREATE KEYSPACE taxi
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 2};

CREATE KEYSPACE taxi
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 1};

DESCRIBE taxi;

USE taxi;

CREATE TABLE taxi_trip (
  trip_id bigint,
  taxi_id int,
  timestamp_ TIMESTAMP,
  distance double,
  PRIMARY KEY (timestamp_)
);

DROP TABLE taxi_trip_distance_per_month_and_taxi;
CREATE TABLE taxi_trip_distance_per_month_and_taxi (
  taxi_id int,
  year_ int,
  month_ int,
  day_ int,
  distance double,
  PRIMARY KEY (year_, month_, day_, taxi_id)
);

SELECT month_, taxi_id, SUM(distance)
FROM taxi_trip_distance_per_month_and_taxi
WHERE year_ = 2013
GROUP BY year_, month_
ORDER BY SUM(distance) DESC;

CREATE TABLE taxi_trip_start_end_points (
  starting_point_latitude double,
  starting_point_longitude double,
  ending_point_latitude double,
  ending_point_longitude double,
  PRIMARY KEY (
    starting_point_latitude,
    starting_point_longitude,
    ending_point_latitude,
    ending_point_longitude
  )
);

SELECT starting_point_latitude, starting_point_longitude
FROM taxi_trip_start_end_points
GROUP BY starting_point_latitude, starting_point_longitude
ORDER BY COUNT(*) DESC
LIMIT 10;

DESCRIBE taxi_trip;

CREATE TABLE taxi_trip (
-- dimensions
  taxi_id int,
  trip_year int,
  trip_month int,
  trip_day int,

-- meta infos
  trip_timestamp timestamp,
  trip_id bigint,
  call_type text,
  origin_call int,
  origin_stand int,
  day_type text,
  missing_data boolean,
  polyline text,

--   faits
  starting_point_latitude double,
  starting_point_longitude double,
  ending_point_latitude double,
  ending_point_longitude double,
  distance double,

--   partition key (trip_year, trip_month, trip_day), clust key trip_id
  PRIMARY KEY ((trip_year, trip_month, trip_day), trip_id)
--   partition key (trip_year, trip_month, trip_day), clust key trip_hour, trip_month, trip_second,trip_id
--   PRIMARY KEY ((trip_year, trip_month, trip_day), trip_hour, trip_month, trip_second,trip_id)
);

CREATE TABLE taxi_trip_shorted (
  trip_id bigint,
  taxi_id int,
  trip_year int,
  trip_month int,
  trip_day int,
  distance double,
  PRIMARY KEY ((trip_year, trip_month, trip_day), trip_id)
);

CREATE TABLE taxi_trip_coordinates (
  trip_id bigint,
  start_latitude double,
  start_longitude double,
  end_latitude double,
  end_longitude double,
  PRIMARY KEY (trip_id)
);