import csv
from collections import namedtuple
from datetime import datetime

import pytz

from Taxi.polyline_parser import parse_polyline

PATH_TO_FILE = "/home/tdancois/PycharmProjects/nf26-td-nosql/Taxi/train.csv"

CompletedTrip = namedtuple("CompletedTrip",
                           ["trip_id",
                            "call_type",
                            "origin_call",
                            "origin_stand",
                            "taxi_id",
                            "timestamp",
                            "day_type",
                            "missing_data",
                            "polyline"])

CALL_TYPES = {'A', 'B', 'C'}
DAY_TYPES = {'A', 'B', 'C'}

# TODO: we might use this
LISBON_TIMESTAMP = pytz.timezone('Europe/Lisbon')


def number_of_lines():
    with open(PATH_TO_FILE) as fdesc:
        return len(fdesc.readlines())


def _int_or_none(value):
    if value in {"NA", ""}:
        return None
    else:
        return int(value)


def _parse_row(row):
    call_type = row["CALL_TYPE"]
    assert (call_type in CALL_TYPES)

    day_type = row["DAY_TYPE"]
    assert (day_type in DAY_TYPES)

    timestamp = int(row["TIMESTAMP"])
    timestamp = datetime.fromtimestamp(timestamp)

    return CompletedTrip(
        trip_id=int(row["TRIP_ID"]),
        call_type=call_type,
        origin_call=_int_or_none(row["ORIGIN_CALL"]),
        origin_stand=_int_or_none(row["ORIGIN_STAND"]),
        taxi_id=int(row["TAXI_ID"]),
        timestamp=timestamp,
        day_type=day_type,
        missing_data=row["MISSING_DATA"] == "True",
        polyline=parse_polyline(row["POLYLINE"]),
    )


def parse_file(skip = 0):
    with open(PATH_TO_FILE, "r") as fdesc:
        reader = csv.DictReader(fdesc)
        for _ in range(skip):
            next(reader)
        for row in reader:
            try:
                yield _parse_row(row)
            except Exception as exception:
                # couldn't parse this row
                raise Exception(exception, row)


if __name__ == "__main__":
    import itertools

    for r in itertools.islice(parse_file(), 5):
        print(r.timestamp)
