import sys

from geopy.distance import vincenty

sys.setrecursionlimit(10000)


# def compute_distance(polyline, previous_point=None, previous_value=0):
#     if len(polyline) == 0:
#         return previous_value
#
#     gps_point = polyline.pop()
#
#     if previous_value is None:
#         return compute_distance(polyline, gps_point)
#     else:
#         distance = vincenty(gps_point, previous_point).kilometers
#         total_distance = previous_value + distance
#         return compute_distance(polyline, gps_point, total_distance)

def compute_distance(polyline):
    distance = 0
    for i in range(0, len(polyline) - 2):
        distance += vincenty(polyline[i], polyline[i+1]).kilometers
    return distance


if __name__ == "__main__":
    polyline = \
        [(41.16402, -8.636823), (41.164029, -8.636796), (41.164029, -8.636787), (41.164029, -8.636787),
         (41.164065, -8.636724), (41.164209, -8.636526), (41.164227, -8.636517), (41.164218, -8.636517),
         (41.164236, -8.636517), (41.164227, -8.636517), (41.164551, -8.637039), (41.165082, -8.638983),
         (41.16573, -8.64018), (41.166846, -8.641548), (41.168097, -8.643087), (41.169087, -8.643825),
         (41.17023, -8.642574), (41.170536, -8.64252), (41.170158, -8.643807), (41.16879, -8.645229),
         (41.170275, -8.647344), (41.169942, -8.649117), (41.170104, -8.650404), (41.171247, -8.651763),
         (41.171886, -8.652591), (41.171868, -8.652573), (41.171868, -8.652564), (41.171868, -8.652564),
         (41.171859, -8.652573), (41.171841, -8.652582), (41.171841, -8.652582), (41.17185, -8.652573),
         (41.171958, -8.652645), (41.172912, -8.654571), (41.172993, -8.654724), (41.173083, -8.654994),
         (41.171706, -8.6562), (41.171274, -8.65656), (41.171031, -8.656767), (41.170815, -8.656704),
         (41.170248, -8.655354), (41.169339, -8.65305)]

    total_distance = compute_distance(polyline)
    print(total_distance)
