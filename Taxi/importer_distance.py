from collections import namedtuple

from Taxi.importer import format_columns_for_query, generic_importer
from Taxi.polyline_distance import compute_distance
from Taxi.reader_taxi import parse_file

Row = namedtuple("Row", [
    "trip_id",
    "taxi_id",
    "trip_year",
    "trip_month",
    "trip_day",
    "distance",
])


def make_rows_for_insertion(skip=0):
    for trip in parse_file(skip=skip):
        if trip.missing_data:
            continue

        polyline = \
            [(point.latitude, point.longitude) for point in trip.polyline]

        if len(polyline) < 8:
            continue

        try:
            distance = compute_distance(polyline)
        except Exception as exception:
            raise Exception(exception, polyline)

        if distance < 0.01:
            continue

        yield Row(
            trip_id=trip.trip_id,
            taxi_id=trip.taxi_id,
            trip_year=trip.timestamp.year,
            trip_month=trip.timestamp.month,
            trip_day=trip.timestamp.day,
            distance=distance,
        )


if __name__ == "__main__":
    total_rows = 1648745

    rows = make_rows_for_insertion()

    print("%d records to insert" % total_rows)

    columns = format_columns_for_query(Row)
    generic_importer("taxi_trip_shorted", columns, rows, total_rows)
