from collections import namedtuple

from geopy.distance import vincenty

from Taxi.reader_taxi import parse_file

Trip = namedtuple(
    "Trip",
    [
        "trip_id",
        "start_latitude",
        "start_longitude",
        "end_latitude",
        "end_longitude",
    ]
)


def get_coordinates(yield_dict=False):
    for trip in parse_file():
        polyline = \
            [(point.latitude, point.longitude) for point in trip.polyline]
        if len(polyline) < 5:
            continue
        first_coordinates = polyline[0]
        last_coordinates = polyline[-1]
        trip = Trip(
            trip_id=trip.trip_id,
            start_latitude=first_coordinates[0],
            start_longitude=first_coordinates[1],
            end_latitude=last_coordinates[0],
            end_longitude=last_coordinates[1],
        )
        if yield_dict:
            yield trip._asdict()
        else:
            yield trip


def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)


def get_start(element):
    return (element["start_latitude"], element["start_longitude"])


def get_end(element):
    return (element["end_latitude"], element["end_longitude"])


def k_means_without_moving_centroids(elements, centroids, nb_clusters=8):
    new_centroids = centroids[:]
    elements_in_clusters = [0 for _ in range(nb_clusters)]

    for element in elements:
        distances_to_centroids = \
            [vincenty(get_start(centroid), get_start(element)).kilometers +
             vincenty(get_end(centroid), get_end(element)).kilometers
             for centroid in centroids]

        closest_centroid = distances_to_centroids.index(min(distances_to_centroids))

        new_centroids[closest_centroid]["start_latitude"] += element["start_latitude"]
        new_centroids[closest_centroid]["start_longitude"] += element["start_longitude"]
        new_centroids[closest_centroid]["end_latitude"] += element["end_latitude"]
        new_centroids[closest_centroid]["end_longitude"] += element["end_longitude"]

        elements_in_clusters[closest_centroid] += 1

    for i in range(nb_clusters):
        new_centroids[i] /= elements_in_clusters[i]

    return new_centroids


if __name__ == "__main__":
    has_changed = True
    elements = get_coordinates(yield_dict=True)
    centroids = [next(elements) for _ in range(8)]

    nb_iterations = 1

    while has_changed is True:
        print("Iteration %d" % nb_iterations)

        new_centroids = k_means_without_moving_centroids(get_coordinates(yield_dict=True), centroids)

        has_changed = centroids != new_centroids
        centroids = new_centroids

        nb_iterations += 1

    print(centroids)
