import itertools

import numpy

from Taxi.reader_taxi import parse_file

completed_trips = list(itertools.islice(parse_file(), 10))

for completed_trip in completed_trips:
    points = \
        numpy.array([(point.latitude, point.longitude) for point in completed_trip.polyline])

    print(numpy.average(points, axis=0))
