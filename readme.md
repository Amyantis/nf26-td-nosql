ssh tunnel to Cassandra:
ssh -NfL 9042:127.0.0.1:9042 e09@nf26.leger.tf

---

Don't use
```
conda create --name nf26-td-nosql
```
Use
```
conda env create -f environment.yml
```

Update your env from environment.yml
```
conda-env  update -f environment.yml
```

---

```
source activate nf26-td-nosql
source deactivate nf26-td-nosql
```
