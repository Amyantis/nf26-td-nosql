# server: nf26.leger.tf
# Initial infos
# login: e09
# password: e09
# ssh command to change password: passwd

# ipython
# cqlsh

# gestionnaire fichier, connexion au server sftp://e09@nf26.leger.tf

import itertools
from datetime import datetime
from io import BytesIO
from zipfile import ZipFile

import requests


def _open_unique_file_in_zip(fdesc):
    zipfile = ZipFile(fdesc)
    zip_names = zipfile.namelist()
    if len(zip_names) == 1:
        file_name = zip_names.pop()
        extracted_fdesc = zipfile.open(file_name)
        return extracted_fdesc
    else:
        raise Exception("More than one file in zip", zip_names)


def _get_zip(file_url):
    url = requests.get(file_url)
    fdesc = BytesIO(url.content)

    return fdesc


from lxml import etree
from collections import namedtuple

Position = namedtuple("Position",
                      ["latitude",
                       "longitude"])
Ouverture = namedtuple("Ouverture",
                       ["debut",
                        "fin",
                        "saufjour"])
Prix = namedtuple("Prix",
                  ["nom",
                   "id",
                   "maj",
                   "valeur"])
Pvd = namedtuple("Pvd",
                 ["id",
                  "position",
                  "cp",
                  "pop",
                  "adresse",
                  "ville",
                  "ouverture",
                  "services",
                  "prix"])


def _make_ouverture(pdv):
    ouverture = etree.SubElement(pdv, "ouverture")
    return Ouverture(
        debut=ouverture.get("debut"),
        fin=ouverture.get("fin"),
        saufjour=ouverture.get("saufjour")
    )


def _float_or_none(value):
    if value == "":
        return None
    else:
        return float(value)


def _parse_pdv(pdv):
    position = Position(
        latitude=_float_or_none(pdv.get("latitude")),
        longitude=_float_or_none(pdv.get("longitude"))
    )
    ouverture = _make_ouverture(pdv)
    services = _make_services(pdv)
    prix = _make_prix(pdv)

    adresse = pdv.xpath("adresse")[0].text
    ville = pdv.xpath("ville")[0].text

    return Pvd(
        id=int(pdv.get("id")),
        position=position,
        cp=int(pdv.get("cp")),
        pop=pdv.get("pop"),
        adresse=adresse,
        ville=ville,
        ouverture=ouverture,
        services=services,
        prix=prix
    )


def _make_prix(pdv):
    prix = [
        Prix(
            nom=prix.get("nom"),
            id=int(prix.get("id")),
            maj=_parse_maj_datetime(prix.get("maj")),
            valeur=float(prix.get("valeur"))
        )
        for prix in pdv.xpath("prix")
    ]
    return prix


def _parse_maj_datetime(value):
    return datetime.strptime(value, "%Y-%m-%d %H:%M:%S")


def _make_services(pdv):
    services = [service.text for service in pdv.xpath("services/service")]
    return services


def _parse(fdesc):
    tree = etree.parse(fdesc)
    for pdv in tree.xpath("/pdv_liste/pdv"):
        yield _parse_pdv(pdv)


def get_records():
    zip_url = "https://donnees.roulez-eco.fr/opendata/instantane"
    prix_carburants_instantane_fdesc = _open_unique_file_in_zip(_get_zip(zip_url))
    yield from _parse(prix_carburants_instantane_fdesc)


if __name__ == "__main__":
    for r in itertools.islice(get_records(), 2):
        print(r)
