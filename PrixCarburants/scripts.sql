-- Une ligne correspond aux prix d'une station

-- http://docs.datastax.com/en/cql/3.3/cql/cql_reference/cql_data_types_c.html

-- Attributs:
--
-- id
-- latitude
-- longitude
-- cp
-- pop
-- adresse
-- ville
-- ouverture
-- services
-- prix

CREATE KEYSPACE carburants
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 2};

DESCRIBE carburants;

USE carburants;

-- DROP TABLE station;

CREATE TABLE prix_carburant (
  id_station int,
  maj date,
  cp int,
  ville text,
  carburant_nom text,
  carburant_id text,
  prix double,
  PRIMARY KEY (id_station, maj)
);

DESCRIBE prix_carburant;